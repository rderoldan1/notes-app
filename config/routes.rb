Rails.application.routes.draw do
  get 'search/index'
  resources :folders, only: :create
  resources :notes, only: :create

  get '*path', to: 'folders#show'


  root to: 'folders#show'
end
