class CreateNote < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.string :name
      t.text :body
      t.belongs_to :folder, foreign_key: true
    end
  end
end
