# frozen_string_literal: true

module Folders
  class Show
    attr_reader :params

    def initialize(params = "")
      @params = params[0] == "/" ? params.sub("/", "") : params
    end

    def note
      folder.notes.find_by(name: paths.last)
    end

    def notes
      folder.notes
    end

    def folder_path?
      parse_folders.last.present?
    end

    def folders
      folder.sub_folders
    end

    def folder
      @folder ||= parse_folders.compact.last || root_folder
    end

    def parse_folders
      current_folder = root_folder

      return [current_folder] if paths.empty?

      paths.map do |path|
        raise("FolderNotFound #{path}") unless current_folder

        current_folder = current_folder.sub_folders.find_by(name: path)
      end
    end

    def root_folder
      @root_folder ||= Folder.root
    end

    def paths
      @paths ||= params.split("/")
    end
  end
end
