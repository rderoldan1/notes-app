class Folder < ApplicationRecord
  has_many :sub_folders, class_name: 'Folder', foreign_key: 'parent_id', dependent: :destroy
  has_many :notes
  belongs_to :parent, class_name: 'Folder'

  scope :root, -> { find_by(parent_id: nil) }

  validates :name, uniqueness: { scope: 'parent_id' }

  def root?
    !parent_id?
  end

  def permalink
    return "/" if root?

    "#{parent.permalink}#{name}/"
  end
end
