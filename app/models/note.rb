class Note < ApplicationRecord
  belongs_to :folder

  validates :name, uniqueness: { scope: 'folder_id' }

  def permalink
    "#{folder.permalink}#{name}"
  end
end
