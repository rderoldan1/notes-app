module Folders
  class SubdirectoriesFinder

    attr_reader :id, :name

    def initialize(id:, name:)
      @id = id
      @name = name
    end

    def find
      Note.where(folder_id: subfolders_ids).where("name LIKE ?", "%#{name}%")
    end

    def subfolders_ids
      Folder.join_recursive do |query|
        query.start_with(id: id).connect_by(id: :parent_id)
      end.pluck(:id)
    end
  end
end

