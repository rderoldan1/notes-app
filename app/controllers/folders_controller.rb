# frozen_string_literal: true

class FoldersController < ApplicationController
  def show
    @folder_service = Folders::Show.new(folder_params.fetch(:path, ""))
  end

  def create
    folder = Folder.create(create_params)

    redirect_to folder.parent.permalink
  end

  private

    def create_params
      params.require(:folder).permit(:parent_id, :name)
    end

    def folder_params
      params.permit(:path)
    end
end
