class SearchController < ApplicationController
  def index
    @notes = if search_params.present?
               Folders::SubdirectoriesFinder.new(search_params).find
             else
               Note.all
             end
  end

  def search_params
    params.permit(:id, :name).to_hash.symbolize_keys
  end
end
