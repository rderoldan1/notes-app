class NotesController < ApplicationController

  def create
    note = Note.create(create_params)

    redirect_to note.permalink
  end

  private

    def create_params
      params.require(:note).permit(:folder_id, :name, :body)
    end
end
